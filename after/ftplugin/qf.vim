" set nowinfixheight nowinfixwidth

function! s:fixtitle()
    let title = getwinvar('', 'quickfix_title', '')[1:]
    let title = substitute(title, '^:rg\zs --vimgrep --no-heading', '', '')
    let title = substitute(title, '^\s*[''"]\(.*\)[''"]\s*$', '\1', '')
    call setwinvar('', 'quickfix_title', title)
endfunction

call s:fixtitle()
setl statusline=%{getwinvar('','quickfix_title','')}%=%y
augroup QfVimconfig
    autocmd!
    autocmd QuickFixCmdPost * call s:fixtitle()
augroup end

setlocal cole=2 cocu=nvic
syntax match QfConceal /^|\+\s*/ conceal
