
" see $VIMDIR/plugin/md2pdf.vim
" see $VIMDIR/autoload/md2pdf.vim

setlocal expandtab softtabstop=4 tabstop=4

setlocal fo-=c

command! -buffer FixDiscrete call <sid>fixDiscrete() <bar> set hls
function! s:fixDiscrete()
	" proofs/expressions
	%s/\s*∃\s*/ \\exists /ge
	%s/\s*∀\s*/ \\forall /ge
	%s/\s*→\s*/ \\rightarrow /ge
	%s/\s*¬\s*/ \\neg /ge
	%s/\s*∧\s*/ \\wedge /ge
	%s/\s*∨\s*/ \\vee /ge
	%s/\s*↔\s*/ \\leftrightarrow /ge
	%s/\s*⇔\s*/ \\Leftrightarrow /ge
	%s/\s*⇒\s*/ \\Rightarrow /ge
	" sets
	%s/\s*∈\s*/ \\in /ge
	%s/\s*∋\s*/ \\ni /ge
	%s/\s*∉\s*/ \\notin /ge
	%s/\s*∩\s*/ \\cap /ge
	%s/\s*∪\s*/ \\cup /ge
	%s/\s*⊂\s*/ \\subset /ge
	%s/\s*⊃\s*/ \\supset /ge
	" random
	%s/\s*≠\s*/ \\neq /ge
	%s/\s*≤\s*/ \\leq /ge
	%s/\s*≥\s*/ \\geq /ge
	%s/\s*≈\s*/ \\approx /ge
	%s/\s*≡\s*/ \\equiv /ge
	%s/\s*⊕\s*/ \\oplus /ge
	%s/\s*∤\s*/ \\nmid /ge
	%s/\s*⋅\s*/ \\cdot /ge
    %s/–/-/ge
	%s/−/-/ge
	%s/[“”]/"/ge
	let @/ = '\%('.join([
	\	'\\exists',
	\	'\\forall',
	\	'\\rightarrow',
	\	'\\neg',
	\	'\\wedge',
	\	'\\vee',
	\	'\\leftrightarrow',
	\	'\\Leftrightarrow',
	\	'\\Rightarrowets',
	\	'\\in',
	\	'\\ni',
	\	'\\notin',
	\	'\\cap',
	\	'\\cup',
	\	'\\subset',
	\	'\\supset',
	\	'\\neq',
	\	'\\leq',
	\	'\\geq',
	\	'\\approx',
	\	'\\equiv',
	\	'\\oplus'
	\], '\|').'\)'
	" norm! ggn
endfunction


" md2pdf
" ~~~~~~

command! -buffer -bar -bang -nargs=0 PdfOpenOnWrite call md2pdf#set_write(<q-bang>, 1)
command! -buffer -bar -nargs=0 NoPdfOpenOnWrite call md2pdf#set_write('', 0)

command! -buffer -nargs=0 OpenPdf call md2pdf#open_pdf()
command! -buffer -bar -nargs=0 TmpPdfWrite call md2pdf#temp_write()
