
source $VIMRUNTIME/ftplugin/man.vim

nnoremap <c-h> :execute "normal! `Z"<cr>
nnoremap <buffer> K :execute "normal! mZ:Man \<lt>c-r>\<lt>c-w>\<lt>cr>"<cr>
vnoremap <buffer> K ymZ:Man <c-r>"<cr>"<cr>

