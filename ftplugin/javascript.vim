
command! FixParams call <sid>fixParams()
function! s:fixParams()
	let [l:lnum, l:col, l:winline] = [line('.'), col('.'), winline() - &scrolloff - 1]
	let l:s = @/
	g/\%(@param.*\n.\{,6}\)\@<!@param/Tabularize jsdocparams
	let @/ = l:s
	call setpos('.', [bufnr('.'), l:lnum, l:col, 0, l:col])
	normal! zvzt
	if (l:winline > 0)
		execute 'normal! '.l:winline."\<c-y>"
	endif
endfunction

command! -bar -range -nargs=0 Comma2Semi let w:_searchsave = @/ <bar> exec '<line1>,<line2>s/\%(\t\%(var\s*\)\@!\|var\s\)\(\w.*\)[,;]/var \1;' <bar> let @/ = w:_searchsave <bar> unlet w:_searchsave

command! JsFold call <sid>jsFold()
function! s:jsFold()
	let [l:lnum, l:col, l:winline, l:searchSave] = [line('.'), col('.'), winline() - &scrolloff - 1, @/]
	silent normal! zX
	g/^\/\*\*/normal! zo
	silent g/\m^\s*\%(function\|\w\+\.prototype\.\w\+\s*=\s*function\|\w\+\.\w\+\s*=\s*function\)/if foldlevel('.')|exec 'norm! zc'|endif
	call setpos('.', [bufnr('.'), l:lnum, l:col, 0, l:col])
	normal! zvzt
	if (l:winline > 0)
		execute 'normal! '.l:winline."\<c-y>"
	endif
	let @/ = l:searchSave
endfunction
nnoremap <silent> zJ :call <sid>jsFold()<cr>

function! s:multiSep(l1, l2)
	for l:line in getline(a:l1, a:l2)
		let [l:rline,l:space] = add(reverse(split(l:line, '^\s*\zs')), '')[:1]
		let l:spline = split(substitute(l:rline, '^var\s\+\|;$', '', 'g'), '\s*=\s*')
		let [l:tokens,l:val] = [l:spline[:-2],l:spline[-1]]
		delete
		call setpos('.', [0, line('.')-1, 1])
		call append('.', map(l:tokens, { token -> l:space.'var '.token.' = '.l:val.';' }))
	endfor
endfunction
command! -bar -range -nargs=0 MultiSep call s:multiSep(<line1>, <line2>)

nnoremap <buffer> <silent> <leader>IL :s&$&\=' // eslint-disable-line '.split(v:statusmsg, ':')[0]&<bar>nohlsearch<cr>

syntax keyword jsCommentNote    contained NOTE
syntax region  jsComment        start=+//+ end=/$/ contains=jsCommentNote,@Spell extend keepend
syntax region  jsComment        start=+/\*+  end=+\*/+ contains=jsCommentNote,@Spell fold extend keepend
syntax region  jsEnvComment     start=/\%^#!/ end=/$/ display
hi def link jsCommentNote Todo

