vim-config
==========

### A Simple Yet Functional Vim Configuration

**_by dylnmc_**

![screenshot of $MYVIMRC](images/vim-screenshot.png)

---
## My ⠈⠢⡠⠊  ⡇ ⢸⠢⡠⢺  ⡟⠻⡀ ⢎⣉

Recently, I cleaned up my vimrc. It is quite plain, but it has all of the
options and plugins that I like! :)

[Check it out](https://bitbucket.org/dylnmc/vim-config/src/master/vimrc) if you
dare.

## The Magic

###### Most of the magic happens in [./plugin/](https://bitbucket.org/dylnmc/vim-config/src/master/plugin/).

### [plugin/color.vim](https://bitbucket.org/dylnmc/vim-config/src/master/plugin/color.vim)

* Small script that merely creates `TGC()` and sets my colors up
	- `TGC()` just sets `'termguicolors'` and `&t_8f` and `&t_8b` if the terminal is not urxvt

### [plugin/lyne-todo.vim](https://bitbucket.org/dylnmc/vim-config/src/master/plugin/lyne-todo.vim)

* A temporary script for my statusline plugin until I get act together

### [plugin/maps.vim](https://bitbucket.org/dylnmc/vim-config/src/master/plugin/maps.vim)

* This script sets up my *leader* as `<space>` and creates some mappings that I use daily
	- **`<c-l>`** **=** _`nohl` & `diffu` & `<c-l>` & refresh syntax from start_
	- **`&mapleader`** **=** `<space>`
	- **`<leader>p`\*** **=** _mappings for **[vim-plug](https://github.com/junegunn/vim-plug)**_
	- _mappings for editing vimrc and sourcing vimrc or current vim script_
	- _(a few other mappings ...)_

### [plugin/plugin-settings.vim](https://bitbucket.org/dylnmc/vim-config/src/master/plugin/plugin-settings.vim)

* This script configures settings for plugins installed via vim-plug:
	- **ale**
		* configure `g:ale_linters`
	- **vim-lyne**
		* even more silly settings for my stl
	- **vim-undotree**
		* a command to open up via `<bslash>u` (`u` for undo!)
	- **ultisnips**
		* My [ultisnips](https://github.com/sirver/UltiSnips) configuration!
	- **vim-javascript**
		- minor adjustments to [vim-javascript](https://github.com/pangloss/vim-javascript)

### [plugin/save.vim](https://bitbucket.org/dylnmc/vim-config/src/master/plugin/save.vim)

* Saving mapped to `<space><space>`! _ahhh.._
	- It doesn't overwrite `<space><space>` if it's already defined, though.
		* You can inoremap `:Save<cr>` to anything if you want

### [plugin/search.vim](https://bitbucket.org/dylnmc/vim-config/src/master/plugin/search.vim)

* remap `*`, `#`, `g*`, `g#` to be case sensitive!
* create other mappings for searching for WORDs and visual selection

### [plugin/synstack.vim](https://bitbucket.org/dylnmc/vim-config/src/master/plugin/synstack.vim)

* map `<c-p>` to print syntax group(s) of word under cursor

### [after/plugin/fold.vim](https://bitbucket.org/dylnmc/vim-config/src/master/after/plugin/fold.vim)

* creates a folding function and assigns it to `'foldtext'`

