
if has('win32')
	set guifont=Consolas:h13
else
	set guifont=DejaVu\ sans\ Mono\ for\ Powerline\ 16.5
endif

set guioptions=i;
function! ToggleGUICruft()
    if &guioptions ==# 'i'
        exec('set guioptions=imTrL')
    else
        exec('set guioptions=i')
    endif
endfunction
nnoremap <silent> <leader>G :call ToggleGUICruft()<cr>

set guicursor+=a:block-Cursor-blinkwait0-blinkoff0-blinkon0

