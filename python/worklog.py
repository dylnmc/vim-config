#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from datetime import datetime
from os import name as osname, mkdir
from os.path import expanduser, isdir, isfile
from sys import argv

if osname == 'nt':
    _dir = '~/_worklog'
else:
    _dir = '~/.worklog'

_dir = expanduser(_dir)

_now = datetime.today()
_logF = '{}/{}_{}_{}.log'.format(_dir, _now.year, _now.month, _now.day)

def main():
    if not isdir(_dir):
        mkdir(_dir)

    logIn = False
    if not isfile(_logF):
        logIn = True
    else:
        try:
            with open(_logF, 'r') as f:
                if f.readlines()[-1].split(':', 1)[0] == 'out':
                    logIn = True
        except:
            logIn = True


    with open(_logF, 'a+') as f:
        f.write('{}:{}:{}:{}\n'.format('in' if logIn else 'out', _now.hour, _now.minute, _now.second))

    print 'logged {0} at {1:02d}:{2:02d}:{3:02d}'.format('in' if logIn else 'out', _now.hour, _now.minute, _now.second)


if __name__ == '__main__':
    main()

