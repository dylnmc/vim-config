#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from datetime import datetime
from os import name as osname
from os.path import expanduser
from vim import command as vicmd

if osname == 'nt':
    _dir = '~/_worklog'
else:
    _dir = '~/.worklog'

_dir = expanduser(_dir)

_now = datetime.today()
_logF = '{}/{}_{}_{}.log'.format(_dir, _now.year, _now.month, _now.day)

def main():
    vicmd('edit {}'.format(_logF))

if __name__ == '__main__':
    main()

