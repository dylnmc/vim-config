#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from datetime import datetime
from os import name as osname, mkdir
from os.path import expanduser, isdir, isfile
from sys import argv

if osname == 'nt':
    _dir = '~/_worklog'
else:
    _dir = '~/.worklog'

_dir = expanduser(_dir)

_now = datetime.today()
_logF = '{}/{}_{}_{}.log'.format(_dir, _now.year, _now.month, _now.day)

def main():
    if not isfile(_logF):
        print("Log file not found. Log in to start working.")
        return

    with open(_logF, 'r') as f:
        log = tuple(tuple(line.strip().split(':')) for line in f.readlines())

    firstLogIn = None
    logIn = None
    seconds = 0
    for line in log:
        try:
            if logIn is None:
                if line[0] == 'out': continue
                if len(line) == 4:
                    logIn = int(line[1]) * 3600 + int(line[2]) * 60 + int(line[3])
                else:
                    logIn = int(line[1])
                if not firstLogIn:
                    firstLogIn = logIn
            else:
                if len(line) == 4:
                    logOut = int(line[1]) * 3600 + int(line[2]) * 60 + int(line[3])
                else:
                    logOut = int(line[1])
                seconds += logOut - logIn
                logIn = None
        except: continue
    try:
        if logIn:
            seconds += (_now.hour * 3600 + _now.minute * 60 + _now.second) - logIn
    except: pass

    secondsRem = 8 * 3600 - seconds

    hours = seconds // 3600
    seconds -= hours * 3600
    minutes = seconds // 60
    seconds -= minutes * 60

    if secondsRem >= 0:
        rem = 'left'
    else:
        secondsRem *= -1
        rem = 'overtime'
    hoursRem = secondsRem // 3600
    secondsRem -= hoursRem * 3600
    minutesRem = secondsRem // 60
    secondsRem -= minutesRem * 60

    if firstLogIn:
        firstLogIn = [0, 0, firstLogIn]
        firstLogIn[0] = firstLogIn[2] // 3600
        firstLogIn[2] -= firstLogIn[0] * 3600
        firstLogIn[1] = firstLogIn[2] // 60
        firstLogIn[2] -= firstLogIn[1] * 60

    print '{}   =>   first login: {}   =>   total hours: {}{}{}   =>   {}'.format(
            _now.strftime('%a %d %b %Y  %H:%M:%S'),
            '{0:02d}:{1:02d}:{2:02d}'.format(*firstLogIn) if firstLogIn else '',
            '{0:02d}:'.format(hours) if hours > 0 else '',
            '{0:02d}:'.format(minutes) if minutes > 0 or hours > 0 else '',
            '{0:02d}{1}'.format(seconds, ' seconds' if hours + minutes == 0 else ''),
            '{0}{1}{2} {3}'.format(
                '{0:02d}:'.format(hoursRem) if hoursRem > 0 else '',
                '{0:02d}:'.format(minutesRem) if minutesRem > 0 or hoursRem > 0 else '',
                '{0:02d}{1}'.format(secondsRem, ' seconds' if hoursRem + minutesRem == 0 else ''),
                rem
                )
            )


if __name__ == '__main__':
    main()

