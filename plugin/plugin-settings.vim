
" plugin/plugin-settings.vim

let g:ale_linters = {
			\ 'javascript': ['eslint'],
			\ 'python': ['pyflakes']
			\ }


let g:lyne_options = {
			\ 'modify_color_left_num': 2,
			\ 'modify_color_right_num': 2,
			\ 'leftSeparator': '',
			\ 'rightSeparator': '',
			\ 'leftDivider': ':.',
			\ 'rightDivider': '.:'
			\ }

" let g:mundo_preview_bottom = 1
" let g:mundo_return_on_revert = 0
let g:undotree_CustomUndotreeCmd  = 'topleft vertical 30 new'
let g:undotree_CustomDiffpanelCmd = 'botright 5 new'
nnoremap <bslash>u :UndotreeToggle<cr>

" tpope/vim-markdown
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh', 'javascript',
			\'help', 'c', 'ruby', 'haskell']

" SirVer/ultisnips
let g:UltiSnipsExpandTrigger         = "<tab>"
let g:UltiSnipsJumpForwardTrigger    = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger   = "<c-k>"
let g:UltiSnipsListSnippets          = "<f1>"
let g:UltiSnipsSnippetsDir           = $VIMDIR.'/plugged/vim-ultisnippets/UltiSnips/'
let g:UltiSnipsSnippetDirectories    = [ "UltiSnips" ]
let g:ultisnips_python_quoting_style = "single"

" pangloss/vim-javascript
let g:javascript_plugin_jsdoc  = 1
let g:javascript_plugin_ngdoc  = 1
let g:jsdoc_allow_input_prompt = 1

" airline
let g:airline_mode_map = {
			\ 'n'      : 'N',
			\ 'no'     : 'N·Operator Pending',
			\ 'v'      : 'V',
			\ 'V'      : 'V·Line',
			\ "\<c-v>" : 'V·Block',
			\ 's'      : 'Select',
			\ 'S'      : 'S·Line',
			\ "\<c-s>" : 'S·Block',
			\ 'i'      : 'I',
			\ 'R'      : 'R',
			\ 'Rv'     : 'V·Replace',
			\ 'c'      : 'Command',
			\ 'cv'     : 'Vim Ex',
			\ 'ce'     : 'Ex',
			\ 'r'      : 'Prompt',
			\ 'rm'     : 'More',
			\ 'r?'     : 'Confirm',
			\ '!'      : 'Shell',
			\ 't'      : 'Terminal'
			\ }

" let g:airline_powerline_fonts = 1
let g:airline#extensions#whitespace#mixed_indent_algo = 2
if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif
let g:airline_symbols.maxlinenr = '␤'
let g:airline_section_a = airline#section#create(['mode', 'crypt', 'paste', '%{&spell ? '' SPELL'' : ''''}', '%{&iminsert==#1 ? '' LMAP'' : &iminsert==#2 ? '' IM'' : ''''}'])
let g:airline_section_b = airline#section#create(['hunks', '%{airline#util#wrap(airline#extensions#branch#get_head(),0)}'])
let g:airline_section_c = airline#section#create(['file'])
let g:airline_section_x = airline#section#create(['filetype'])
let g:airline_section_y = airline#section#create(['%{&fenc} %{&ff}'])
let g:airline_section_z = airline#section#create(['%2p%%%3c'])

" magit
" let g:magit_show_magit_mapping = '<nop>'

