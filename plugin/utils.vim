
" Each function below that returns a binary or hexadecimal value can be
" optionally prefixed with 0b or 0x, respectively. To enable it on a per-buffer
" basis, use :BinPrefix and HexPrefix, resp. If you just need it one time, the
" last argument for the functions that return binary or hexadecimal values is
" optional: if it is 0, never prefix; if it is 1, always prefix.

" prefix 0b?
" :BinPrefix -> Enable binary prefix in buffer
" :BinPrefix! -> DIsable binary prefix in buffer
command! -nargs=0 -bang BinPrefix let b:binPrefix = <bang>1 <bar> exec 'echo ''Binary Prefix '.(get(b:, 'binPrefix', 1) ? 'En' : 'Dis').'abled'''

" prefix 0x?
" :HexPrefix -> Enable hexadecimal prefix in buffer
" :HexPrefix! -> Disable hexadecimal prefix in buffer
command! -nargs=0 -bang HexPrefix let b:binPrefix = <bang>1 <bar> exec 'echo ''Hex Prefix '.(get(b:, 'hexPrefix', 1) ? 'En' : 'Dis').'abled'''

" DecToBin(5) -> [0b]101 (0b prepended if BinPrefix is enabled)
" DecToBin(5, 6) -> [0b]000101 (0b prepended if BinPrefix is enabled)
" DecToBin(5, 6, 0) -> 000101
" DecToBin(5, 6, 1) -> 0b000101
function! DecToBin(d, ...)
    let l:b = printf('%b', substitute(a:d, '\s', '', 'g'))
    return (get(a:000, 1, get(b:, 'binPrefix', 0)) ? '0b' : '').repeat( 0, max([ 0, str2nr(get(a:000, 0, 0))-strlen(l:b) ]) ).l:b
endfunction

" DecToHex(20) -> [0x]14 (0x prepended if HexPrefix is enabled)
" DecToHex(20, 4) -> [0x]0014 (0x prepended if HexPrefix is enabled)
" DecToHex(20, 4, 0) -> 0014
" DecToHex(20, 4, 1) -> 0x0014
function! DecToHex(d, ...) " decimal to hex (remove all whitespace first)
    let l:h = printf('%x', substitute(a:d, '\s', '', 'g'))
    return (get(a:000, 1, get(b:, 'hexPrefix', 0)) ? '0x' : '').repeat( 0, max([ 0, str2nr(get(a:000, 0, 0))-strlen(l:h) ]) ).l:h
endfunction

function! BinToHex(b) " if a:b is a string, treat as binary; if a:b is a number, treat it like a decimal number
    return ToHex('0b'.(type(a:b) ==# 1 ? substitute(substitute(a:b, '^0b', '', ''), '[^01]', '', 'g') : printf('%b', str2nr(a:b))))
endfunction

" ~n
" BinVert(0b11) -> [0b]00 (0b prepended if BinPrefix is enabled)
" BinVert(0b0011, 4) -> [0b]1100 (0b prepended if BinPrefix is enabled)
" BinVert(0b0011, 4, 0) -> 1100
" BinVert(0b0011, 4, 1) -> 0b1100
function! BinVert(b, ...)
    return (get(a:000, 1, get(b:, 'binPrefix', 1)) ? '0b' : '').join(map(split(DecToBin(a:b, max([strlen(printf('%b', a:b)), str2nr(get(a:000, 0, 0))]), 0), '\zs'), { i,n -> [1,0][n] }), '')
endfunction

" ~n + 1
" TwosComplement(0b1010) -> [0b]0110 (0b prepended if BinPrefix is enabled)
" TwosComplement(0b1010, 8) -> [0b]11110110 (0b prepended if BinPrefix is enabled)
" TwosComplement(0b1010, 8, 0) -> 11110110
" TwosComplement(0b1010, 8, 1) -> 0b11110110
function! TwosComplement(n, ...)
    let l:len = max([1, get(a:000, 0, len(printf('%b', a:n)))])
    return (get(a:000, 1, get(b:, 'binPrefix', 1)) ? '0b' : '').printf('%0'.l:len.'b', eval('0b'.tr(printf('%0'.l:len.'b', a:n)[:l:len-1], '01', '10'))+1)[:l:len-1]
endfunction

" n << shamt
" ShiftLeft(10, 2) -> 40
function! ShiftLeft(n, shamt)
    return float2nr(str2nr(a:n) * pow(2, str2nr(a:shamt)))
endfunction

" n >> shamt
" ShiftRight(10, 2) -> 2
function! ShiftRight(n, shamt)
    return float2nr(str2nr(a:n) / pow(2, str2nr(a:shamt)))
endfunction

" get a macro as an escaped string
" TODO: this is a work in progress
function! GetMacro(r)
    let l:r = getreg(a:r)
    let l:a = char2nr('a')
    let l:chars = map(range(l:a, char2nr('z')), { i,c -> eval('"\<c-'.nr2char(c).'>"') })
    for l:i in range(len(l:chars))
        let l:r = substitute(l:r, l:chars[l:i], '\\<c-'.nr2char(l:i + l:a).'>', 'g')
    endfor
    let l:others = [',', '.', '/', ';', '''', ']', '\', '`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '<', '>', '?', ':', '"', '{', '}', '|', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+' ]
    let l:chars = map(range(len(l:others)), { i -> eval('"\<c-'.escape(l:others[i], '"').'>"') })
    for l:i in range(len(l:others))
        let l:r = substitute(l:r, l:chars[l:i], '\\<c-'.l:others[l:i].'>', 'g')
    endfor
    let l:r = substitute(l:r, "\<esc>", '\\<esc>', 'g')
    return l:r
endfunction

