
" plugin/search.vim

" STAR SEARCH
" ~~~~~~~~~~~
" better ['*', '#', 'g*', 'g#']
" 	1. case sensitive
" 	2. # and g# no longer use '?'
" 		- instead of using '?' use '/' (to preserve 'n' vs 'N' functionality)
" 		- hastle to remember to press 'n' vs 'N' (when used '*' vs '#')
" 	3. show cursor positino at end (vanilla * # g* g# do this too if you tell it
" 	   to do that with set foldopen+=search)

" blatantly overwrite * # g* g#
nnoremap <silent> * :call <sid>StarSearch(1, 1)<bar>set hlsearch<cr>zv
nnoremap <silent> # :call <sid>StarSearch(0, 1)<bar>set hlsearch<cr>zv
nnoremap <silent> g* :call <sid>StarSearch(1, 0)<bar>set hlsearch<cr>zv
nnoremap <silent> g# :call <sid>StarSearch(0, 0)<bar>set hlsearch<cr>zv

" "StarSearch()": searches for word under cursor
" 	@param {bool} forward if true: search forwards; else: search backwards
" 	@param {bool} bounds  if true: prefix word with \< and postfix with \>;
"               otherwise: do not add boundaries for vim regex
function! <sid>StarSearch(forward, bounds)
	let l:cword = expand('<cword>')
	if a:bounds
		let l:word = '\C\<'.l:cword.'\>'
	else
		let l:word = '\C'.l:cword
	endif
	if (! a:forward)
		execute 'normal! '.len(l:cword).'h'
	endif
	let @/ = l:word
	call histadd('/', l:word)
	call search(l:word, 's'.(a:forward ? '' : 'b'))
endfunction


" WORD SEARCH
" ~~~~~~~~~~~
" search for WORDs (only spaces considered boundaries).
" also, use \V for very-no-magic
" finally, escape '\' and '/' since matching WORD
" NOTE: this adheres to ignorecase/smartcase settings

" "WORDSearch()": search for WORD under cursor (only spaces considered
" boundaries)
" 	@param {bool} forward if true: search forwards; else: search backwards
function! <sid>WORDSearch(forward)
	let l:line = getline('.')
	let l:col = col('.')-1
	let l:word = '\V'.substitute(l:line[:l:col-1], '^.*[\ \t]', '', '')
	let l:word .= escape(
		\ substitute(l:line[l:col:], '[\ \t]*\([^\ \t]\+\)[^$]*', '\1', ''),
		\ '\/'
	\)
	let @/ = l:word
	call histadd('/', l:word)
	call search(l:word, 's'.(a:forward ? '' : 'b'))
endfunction

" expose <plug>(wordSearchForward) and <plug>(wordSearchBackward)
nnoremap <plug>(wordSearchForward) :call <sid>WORDSearch(1)<bar>set hlsearch<cr>
nnoremap <plug>(wordSearchBackward) :call <sid>WORDSearch(0)<bar>set hlsearch<cr>

" map defaults (<bslash>* and <bslash>#) only if above plugs not already mapped
" to something
if !hasmapto('<plug>(wordSearchForward)')
	nmap <silent> <bslash>* <plug>(wordSearchForward)
endif
if !hasmapto('<plug>(wordSearchBackward)')
	nmap <silent> <bslash># <plug>(wordSearchBackward)
endif


" VISUAL SEARCH
" ~~~~~~~~~~~~~
" search for visual selection.
" in order to do this:
" 	1. set very-no-magic with \V
" 	2. escape all '\' and '/' so that they don't mess up the search

" expose <plug>(visualSearchForward) and <plug>(visualSearchBackward)
vnoremap <plug>(visualSearchForward) y/\V<c-r>=escape(@",'\/')<cr><cr>:set hlsearch<cr>
vnoremap <plug>(visualSearchBackward) y?\V<c-r>=escape(@",'\/')<cr><cr>:set hlsearch<cr>

" map defaults to <bslash>* and <bslash># only if above plugs not already
" mapped to something
if !hasmapto('<plug>(visualSearchForward)')
	vmap <silent> <bslash>* <plug>(visualSearchForward)
endif
if !hasmapto('<plug>(visualSearchBackward)')
	vmap <silent> <bslash>? <plug>(visualSearchBackward)
endif

