
" see $VIMDIR/autoload/md2pdf.vim
" see $VIMDIR/ftplugin/markdown.vim

augroup md2pd
    autocmd!
    autocmd BufWritePost *.md call md2pdf#convert()
augroup end

