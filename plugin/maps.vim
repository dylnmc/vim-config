
" plugin/maps.vim

" <c-l> is a bit more useful (nohl && diffu && <c-l> && refresh syntax from start)
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nnoremap <silent> <c-l> :nohlsearch<bar>diffupdate<bar>execute "normal! \<lt>c-l>"<bar>syntax sync fromstart<cr>

" <space> as leader
" ~~~~~~~~~~~~~~~~~
nnoremap <space> <esc>
let mapleader = ' '

" plug mappings
" ~~~~~~~~~~~~~
nnoremap <leader>pi :source $MYVIMRC<bar>PlugInstall<cr>
nnoremap <leader>pc :source $MYVIMRC<bar>PlugClean<cr>
nnoremap <leader>pu :source $MYVIMRC<bar>PlugUpdate<cr>
nnoremap <leader>pg :source $MYVIMRC<bar>PlugUpgrade<cr>
" I (used to :D) have a hard time finding plug#begin w/o searching >:|
nnoremap <leader>pp gg:call search('call plug#begin')<cr>zt

" lazy scrolling for :help and such
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nnoremap <down> <c-e>j
nnoremap <up> <c-y>k

" horiz scrolling for the times you have 'nowrap' set
" buffer 'scrolling' for the times you have 'wrap' set (am I weird?)
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nnoremap <expr> <left>  &wrap ? ':bprev<cr>' : 'zh'
nnoremap <expr> <right> &wrap ? ':bnext<cr>' : 'zl'

" Expand wildmenu but do not insert any completion opts.
" (Use in conjunction with <c-i>)
" make sure to set wildcharm=<c-z>
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cnoremap <c-o> <c-e><c-z><left>

" sourcing (and edit $VIMRC) mappings
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nnoremap <silent> <bslash>v :edit $MYVIMRC<bar>call feedkeys("\<lt>c-l>:echo 'editing vimrc'\<lt>cr>")<cr>
nnoremap <silent> <bslash>V :source $MYVIMRC<bar>call <sid>SourceVimPlugins()<bar>call feedkeys("\<lt>c-l>:echo 'vimrc sourced'\<lt>cr>")<cr>
nnoremap <silent> <bslash>S :source %<bar>call feedkeys("\<lt>c-l>:echo expand('%:.').' sourced'\<lt>cr>")<cr>
nnoremap <silent> <bslash>P :update<bar>pyf %<cr>
if (!exists('*<sid>SourceVimPlugins'))
	" only created once (E127 otherwise)
	function! <sid>SourceVimPlugins()
		for l:f in split(globpath($VIMDIR.'/plugin/', '*'), '\n')
			execute 'source ' . fnamemodify(l:f, ':p')
		endfor
	endfunction
endif

" buffer/arg mappings (minor convenience)
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nnoremap <leader>b :buffers<cr>:b<leader>
nnoremap <plug>ArgSwitch :call <sid>ArgSwitch()<cr>
if (!hasmapto('<plug>ArgSwitch'))
	nmap <leader>a <plug>ArgSwitch
endif

function! <sid>ArgSwitch()
	let l:i = 1
	let l:argid = argidx() + 1
	echon ':args'
	for arg in argv()
		if (l:i ==# l:argid)
			echon "\n".l:i.'» ['.arg.']'
		else
			echon "\n".l:i.'» '.arg
		endif
		let l:i += 1
	endfor
	call feedkeys(':argu ', 'n')
endfunction

" automagical "/* | */" completion (when "/*" typed)
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
inoremap <silent> * <c-r>=getline('.')[col('.')-2]==#'/'?"*  */\<lt>left>\<lt>left>\<lt>left>":'*'<cr>

" quickhl
" ~~~~~~~
" use norm "gmiw", for example, to mark word
nmap gm <plug>(operator-quickhl-manual-this-motion)
xmap gm <plug>(operator-quickhl-manual-this-motion)
omap gm <plug>(operator-quickhl-manual-this-motion)
" also use norm "gmm" to mark word (minor convenience)
nmap gmm <plug>(quickhl-manual-this)
" * use norm "gmc" to clear the quickhls
nmap gmc <plug>(quickhl-manual-reset)
nmap gm] <plug>(quickhl-tag-toggle)

" also support norm! "gm" just cuz
nnoremap gM gm

" inner-line-wise mappings
" ~~~~~~~~~~~~~~~~~~~~~~~~
" "in line" (like line-wise but ignore newline at end as well as trailing/leading
" space)
xnoremap <silent> il ^og_
onoremap <silent> il :<c-l>normal! ^vg_<cr>
" "around line" (like line-wise but ignore newline)
xnoremap <silent> al 0o$
onoremap <silent> al :<c-l>normal! 0v$<cr>

function! BeforeEquals(toBeg, xMap)
	let l:line = getline('.')
	if (l:line !~# '=')
		" line does not contain an equals sign
		return
	endif
	if (l:line =~# '^\s*=')
		" equals sign at beginning of line
		if (!a:xMap)
			execute "normal! I  \<esc>hv"
		endif
		return
	endif
	" first, place cursor before first equals sign
	normal! ^t=
	if (l:line[col('.')-1] =~# '\s')
		" if currently on space, move to end of last non-space char
		normal! ge
	endif
	if (a:toBeg)
		" "around" (entire line before equals sign)
		normal! v_
		return
	endif
	" figure out how many characters to visually select
	let l:lastWordPos = match(l:line[:col('.')-1], '\S\+$')
	if (l:lastWordPos ==# -1)
		let l:lastWordPos = 1
	endif
	let l:lastWordPos = col('.') - l:lastWordPos - 1
	echom l:lastWordPos
	if (l:lastWordPos <= 0)
		" if 0 (or below for some weird reason), only select this char
		normal! v
	else
		" otherwise, select all of non-whitespace chars
		execute 'normal! v'.l:lastWordPos.'h'
	endif
endfunction
" "inside Equals" (just first WORD before equals sign)
xnoremap <silent> ie :<c-u>call BeforeEquals(0, 1)<cr>
onoremap <silent> ie :<c-u>call BeforeEquals(0, 0)<cr>
" "around equals" (everything before equals sign excluding whitespace)
xnoremap <silent> ae :<c-u>call BeforeEquals(1, 1)<cr>
onoremap <silent> ae :<c-u>call BeforeEquals(1, 0)<cr>

" store output of command into @+ register
cnoremap <c-bslash> redir @+ <bar> silent  <bar> redir end<s-left><s-left><s-left><left>

" work log
" ~~~~~~~~
" mnemonic: "work time" (print hours worked and such)
nnoremap <silent> <leader>wt :pyfile $VIMDIR/python/workleft.py<cr>
" mnemonic: "work log" (log in -or- log out)
nnoremap <silent> <leader>wl :pyfile $VIMDIR/python/worklog.py<cr>
" convenience (Pause/Break key on black widow tournament ed. by razor)
nnoremap <silent> <F21> :pyfile $VIMDIR/python/worklog.py<cr>
" mnemonic: "work open" (open current log)
nnoremap <silent> <leader>wo :pyfile $VIMDIR/python/workopen.py<cr>

" some (not so) useful folding maps
" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nnoremap <expr> z4 'zz'.(&lines / 4).'<c-e>'
xnoremap <expr> az foldclosed('.') == -1 ? 'zcVzogvo' : 'Vzogvo'
nnoremap zx zXzvzz
nnoremap <expr> zO foldclosed('.') == -1 ? 'zczO' : 'zO'

" when I "Q" just exit for the love of bananas
" (unless I have unsaved changes, in which case please do not :S)
nnoremap Q :qa<cr>
command! Q qa
command! WQ wqa

