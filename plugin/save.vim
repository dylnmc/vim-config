
" expose command
command! Save call <sid>save()

function! s:save()
    if (! &modified)
        " unsilent echon "\ralready saved"
        return
    endif
    " unsilent echon "\rsaving..."
    if (! &write)
        unsilent echon "\rFile not writeable. Save anyway? [Yn] "
        let l:yn = nr2char(getchar())
        if (l:yn !=# 'n' && l:yn !=# 'N')
            set write
            silent update!
            " unsilent echon "\r(set write) saved!"
        else
            redraw
            unsilent echon "\rNOT saved!"
        endif
        return
    endif
    try
        silent update
        " unsilent echon "\rsaved!"
    catch /^Vim\%((\a\+)\)\=:E505/
        silent update!
        unsilent echohl WarningMsg
        unsilent echon "\r(force) saved!"
        unsilent echohl NONE
    catch /^Vim\%((\a\+)\)\=:E32/
        unsilent echohl ErrorMsg
        unsilent echon "\rERROR: No Filename Provided"
        unsilent echohl NONE
    endtry
endfunction

" " map to <space><space> if <space><space> not already mapped
" " don't map if g:save_map is 0
" if (get(g:, 'save_map', 1) != 0)
"     if empty(maparg('<space><space>'))
"         nnoremap <silent> <space><space> :call <sid>save()<cr>
"     endif

"     " also map <s-space><s-space> for gvim
"     if empty(maparg('<s-space><s-space>'))
"         nnoremap <silent> <s-space><s-space> :call <sid>save()<cr>
"     endif

"     " also map <s-space><space> and <space><s-space>
"     if empty(maparg('<s-space><space>'))
"         nnoremap <silent> <s-space><space> :call <sid>save()<cr>
"     endif
"     if empty(maparg('<space><s-space>'))
"         nnoremap <silent> <space><s-space> :call <sid>save()<cr>
"     endif
" endif
