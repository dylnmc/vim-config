
" plugin/save.vim

" expose command
command! Save call <sid>Save()

function! <sid>Save()
	if (! &modified)
		echon "\ralready saved"
		return
	endif
	echon "\rsaving..."
	if (! &write)
		echon "\rFile not writeable. Save anyway? [Yn] "
		let l:yn = nr2char(getchar())
		if (l:yn !=# 'n' && l:yn !=# 'N')
			set write
			silent update!
			echon "\r(set write) saved!"
		else
			redraw
			echon "\rNOT saved!"
		endif
		return
	endif
	try
		silent update
		echon "\rsaved!"
	catch /^Vim\%((\a\+)\)\=:E505/
		silent update!
		echon "\r(force) saved!"
	catch /^Vim\%((\a\+)\)\=:E32/
		echon "\rERROR: No Filename Provided"
	endtry
endfunction

" map to <space><space> if <space><space> not already mapped
" don't map if g:save_map is 0
if get(g:, 'save_map', 1) != 0 && empty(maparg('<c-p>'))
	nnoremap <space><space> :Save<cr>
endif
