
" see $VIMDIR/autoload/textobjs.vim

" "in line" (entire line sans white-space; cursor at beginning--ie, ^)
xnoremap <silent> il :<c-u>normal! g_v^<cr>
onoremap <silent> il :<c-u>normal! g_v^<cr>

" "around line" (entire line sans trailing newline; cursor at beginning--ie, 0)
xnoremap <silent> al m`$o0
onoremap <silent> al :<c-u>normal! m`$v0<cr>

" "in document" (from first line to last; cursor at top--ie, gg)
xnoremap <silent> id :<c-u>normal! G$Vgg0<cr>
onoremap <silent> id :<c-u>normal! GVgg<cr>

" "around fold" (from first line in current fold to last; cursor on first line)
xnoremap az :<c-u>call textobjs#az(1)<cr>
onoremap az :<c-u>call textobjs#az(0)<cr>

" "go (to) other" (align this corner to other corner)
xnoremap <silent> go :<c-u>call textobjs#visualGoOther(0)<cr>

" "get Other" (align Other corner to this corner)
xnoremap <silent> gO :<c-u>call textobjs#visualGoOther(1)<cr>

" "go (to) line" (move this corner to other corner's line)
xnoremap <silent> gl :<c-u>call textobjs#visualGoLine(0)<cr>

" "get Line" (move other line to this corner's line)
xnoremap <silent> gL :<c-u>call textobjs#visualGoLine(1)<cr>

" "inside equals" (just first WORD before equals sign)
xnoremap <silent> ie :<c-u>call textobjs#beforeEquals(0, 1)<cr>
onoremap <silent> ie :<c-u>call textobjs#beforeEquals(0, 0)<cr>

" "around equals" (everything before equals sign excluding white-space)
xnoremap <silent> ae :<c-u>call textobjs#beforeEquals(1, 1)<cr>
onoremap <silent> ae :<c-u>call textobjs#beforeEquals(1, 0)<cr>

" "in number" (next number after cursor on current line)
xnoremap <silent> in :<c-u>call textobjs#inNumber()<cr>
onoremap <silent> in :<c-u>call textobjs#inNumber()<cr>

" "around number" (next number on line and possible surrounding white-space)
xnoremap <silent> an :<c-u>call textobjs#aroundNumber()<cr>
onoremap <silent> an :<c-u>call textobjs#aroundNumber()<cr>

" "in indentation" (indentation level sans any surrounding empty lines)
xnoremap <silent> ii :<c-u>call textobjs#inIndentation()<cr>
onoremap <silent> ii :<c-u>call textobjs#inIndentation()<cr>

" "around indentation" (indentation level and any surrounding empty lines)
xnoremap <silent> ai :<c-u>call textobjs#aroundIndentation()<cr>
onoremap <silent> ai :<c-u>call textobjs#aroundIndentation()<cr>

