
" plugin/color.vim

set background=dark
let g:nord_uniform_diff_background = 1
let g:nord_uniform_status_lines    = 1
let g:nord_comment_brightness = 4
if has('termguicolors')
	function! TGC()
		if ($TERM !~ 'rxvt' && has("termguicolors"))
			let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
			let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
			set termguicolors
		endif
	endfunction
	call TGC()
endif
colorscheme nord
" colorscheme gruvbox

" main bg
let g:nord_airline_normal_main_bg = 9
let g:nord_airline_visual_main_bg = 15
" right
let g:nord_airline_normal_right_bg = 1
let g:nord_airline_normal_right_fg = 6
let g:nord_airline_insert_right_bg = 1
let g:nord_airline_insert_right_fg = 6
let g:nord_airline_replace_right_bg = 1
let g:nord_airline_replace_right_fg = 6
let g:nord_airline_visual_right_bg = 1
let g:nord_airline_visual_right_fg = 6
let g:nord_airline_inactive_right_bg = 1
let g:nord_airline_inactive_right_fg = 6
" middle bg
let g:nord_airline_normal_middle_bg   = 3
let g:nord_airline_insert_middle_bg   = 3
let g:nord_airline_replace_middle_bg  = 3
let g:nord_airline_visual_middle_bg   = 3
let g:nord_airline_inactive_middle_bg = 3

