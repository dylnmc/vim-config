
if !get(g:, 'atWork')
    finish
endif

" F11 fullscreen on windows
nnoremap <f11> :call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)<cr>
inoremap <f11> <c-o>:call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)<cr>
nnoremap <Del> :call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0) <bar> exec 'simalt ~n'<cr>:call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0) <bar> exec 'simalt ~n'<cr>

" augroup GvimWin7
"   autocmd!
"   " fullscreen as soon as any file is opened
"   " autocmd VimEnter * call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)
"   " autocmd VimEnter * simalt ~x
"   " autocmd VimEnter * if <sid>isTutorial() | call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0) | else | set columns=94 lines=25 | endif
" augroup end

nnoremap <f12> :simalt ~x<cr>
inoremap <f12> :simalt ~x<cr>
nnoremap <f10> :set columns=94 lines=25<cr>
inoremap <f10> :set columns=94 lines=25<cr>

function! WorkAddFiles(force)
    if ! a:force && expand('%:p:h:t') !~# '^mobile'
        echoerr 'Doesn''t look like you''re in a work directory'
        return
    endif
    let l:proj_name = substitute(expand('%:p:h:h:t'), '[-_][^-_]\+$', '', '')
    if index(argv(), expand('%:.')) ==# -1
        $argadd %
    endif
    " css
    let l:files = glob('styles/*.css', 0, 1)
    let l:file = printf('styles/%s.css', l:proj_name)
    if filereadable(l:file)
        execute '$argadd '.l:file
        call remove(l:files, index(l:files, l:file))
    endif
    if ! empty(l:files)
        execute '$argadd '.join(l:files)
    endif
    " js
    let l:files = glob('js/*.js', 0, 1)
    let l:file = printf('js/%s.js', l:proj_name)
    if filereadable(l:file)
        execute '$argadd '.l:file
        call remove(l:files, index(l:files, l:file))
    endif
    if ! empty(l:files)
        execute '$argadd '.join(l:files)
    endif
    " data
    silent! $argadd data/*
    " ../index.html
    $argadd ../index.html
    " uncomment to list all args
    "args
    " fix file names
    " silent vimgrep /src\s*=\s*"\zs[^/][^"]*\.\w\{,4}\ze"/jg %
    " let l:paths = map(getqflist(), { _,val -> substitute(val.text[val.col-1:], '".*', '', '') })
    " let l:nefs = [] " non-existant files
    " for l:path in l:paths
    "   let l:comp_file = getcompletion(l:path, 'file')
    "   if len(l:comp_file)
    "       let l:comp_file = l:comp_file[0]
    "       if l:path[0] !=# l:comp_file
    "           execute 'silent %s/\V'.escape(l:path, '\/').'/'.substitute(l:comp_file, '[~&\\/]', '\\&', 'g').'/g'
    "       endif
    "   else
    "       call add(l:nefs, l:path)
    "   endif
    " endfor
    " silent update
    " if len(l:nefs)
    "   echohl ErrorMsg
    "   echom 'Non-existant file'.(len(l:nefs)-1 ? 's' : '').': '.join(map(l:nefs, { _,v -> '"'.v.'"' }), ', ')
    "   echohl NONE
    " endif

    " if only 1 window and 1 tab and current window =~# '\.html$', create proper splits
    if winnr('$') ==# 1 && tabpagenr('$') ==# 1 && expand('%:e') ==# 'html'
        vert snext
        tab snext
        tab slast
        tabprev
        norm! zv
    endif
endfunction

nnoremap <silent> <leader>waa :call WorkAddFiles(0)<cr>
nnoremap <silent> <leader>waA :call WorkAddFiles(1)<cr>

" " automagically open all the files I want for files that look like the projects
" " I work on a lot
" augroup Work
"   autocmd!
"   autocmd VimEnter *.html if (<sid>isTutorial() && argc() ==# 1) | call <sid>openWork() | endif
" augroup end

" let s:tutorialDirs = [ 'Micro', 'Nikon', 'Olympus' ]
" function! <sid>isTutorial()
"   let l:fmatch = '^'.expand('%:t:r')
"   if ((expand('%:p:h:t') =~# l:fmatch
"     \ || expand('%:p:h:h:t') =~# l:fmatch
"     \ || expand('%:p:h:h:h:t') =~# l:fmatch
"     \ || expand('%:p:h:h:h:h:t') =~# l:fmatch)
"     \ && expand('%:p') =~# '^X:\/Sites\/Java\/html5\/\('.join(s:tutorialDirs, '\|').'\)'
"     \ && expand('%') =~# '\.html')
"       return 1
"   endif
"   return 0
" endfunction

" function! <sid>openWork()
"   if (!(<sid>isTutorial() || argc() ==# 1))
"       return
"   endif
"   call WorkAddFiles()
"   silent call feedkeys(':buffer js/'.expand('%:t:r').".js\<cr>", 'n')
" endfunction

" nnoremap <leader>B :execute 'terminal bash' <bar> wincmd T<cr>

" ctags
nnoremap <leader>c :AsyncDo C:/Users/dmcclure/Downloads/ctags.exe -R .

function! WorkFixIndex()
    if empty(@+)
        echoerr 'Nothing in + register'
        return
    endif

    execute 'write '.expand('%:p:h').'/index-java.html'

    %s/$//e

    if search('Body Text Goes Here')
        delete
    endif

    if search('\C^<APPLET\s*\n\?\s*\(ARCHIVE\|CODEBASE\)\|\C^<OBJECT\s\+classid=[^\n]\+\n\s*<PARAM')
        " normal V}2kd"+P_WWf.F/lyt.bbgriwF"li/htmltutorials/source/
        normal V}2kd"+P_WWf.F/lyt.bbgriwF"li/html5/tutorials/
        normal! ==
    endif

    " no point in saving since I have to fix path (nearly) every time anyway
    " silent Save
endfunction
" mnemonic: "work fix index"
nnoremap <leader>wfi :call WorkFixIndex()<cr>

function! WorkCopyIndex()
    if expand('%:t') !=# 'index.html'
        if filereadable('../index.html')
            edit ../index.html
        elseif filereadable('index.html')
            edit index.html
        else
            echom 'C'
            echoerr 'index.html not found :( ... try navigating to proper dir with :lcd or navigating to that buffer'
            return
        endif
    endif
    call search('\c<iframe')
    normal! _wguiwW.W.W.W.W.W.W.W.f<w."+yy
    silent Save
endfunction
" mnemonic: "work copy index"
nnoremap <leader>wci :call WorkCopyIndex()<cr>

function! WorkTranslateTrello()
    if empty(@+)
        echoerr 'Nothing in + register'
        return
    endif
    let l:cu = undotree().seq_cur " current undo
    normal! "+PO
    call search('^\s*\[Olympus\s\+Magnet]')
    normal! dkGp+ci]HTML Live
    normal! ykP3+ci]Java Live
    normal! $F.i-java
    normal! 3k
    -1put_
    normal! o---
    %s/^\s*\[HTML\]/\[HTML src\]/e
    normal! gg"_dd"+yG
    silent execute 'undo '.l:cu
endfunction
" mnemonic: "work translate trello"
nnoremap <leader>wtt :call WorkTranslateTrello()<cr>

function! WorkOlyTrello() " u wot m8?
    let @+ = "\n**[Olympus HTML5 *(Source)*](".@+.')**'
endfunction
" mnemonic: call WorkOlyTrello
nnoremap <leader>wot :call WorkOlyTrello()<cr>

function! WorkReadLive()
    let l:liveFile = substitute(expand('%:p'), 'Feeder\ze\/', '', '')
    if (l:liveFile ==# expand('%:p'))
        echoerr 'Doesn''t look like you''re on the feeder site @_@'
        return
    endif
    if filereadable(l:liveFile)
        normal! gg"_dG
        execute '0read '.l:liveFile
        return
    endif
    echoerr 'No such file. Feeder not synchronised with live ._.'
endfunction
" mnemonic: "work read live"
nnoremap <leader>wrl :call WorkReadLive()<cr>

function! WorkWriteLive()
    let l:f = expand('%:p')
    if (l:f !~# 'Feeder\/')
        echoerr 'Doesn''t look like you''re on the feeder site @_@'
        return
    endif
    let l:liveFile = substitute(l:f, 'Feeder\ze\/', '', '')
    if !filereadable(l:liveFile)
        echom 'Live site file not found ._.'
        return
    endif
    silent execute 'edit '.l:liveFile
    silent normal! gg"_dG
    silent execute '0read '.l:f

    " echohl WarningMsg
    " echon 'Editing updated live html; saving will overwrite live site!'
    " echohl NONE

    " be careful xD
    Save
endfunction
" mnemonic: "work write live"
nnoremap <leader>wwl :call WorkWriteLive()<cr>

let g:workTagList = ['HEAD', 'DIV', 'SCRIPT', 'LINK', 'BODY', 'HTML', 'STYLE',
\ 'TITLE', 'CENTER', 'IFRAME', 'IMG', 'LABEL', 'OPTION', 'SELECT', 'CANVAS',
\ 'P', 'BR', 'SPAN', 'B', 'OBJECT', 'EMBED', 'PARAM', 'TABLE' ]
\ + map(range(1, 6), { _,n -> 'H'.n })
let g:workAttrList = [ 'CLASS', 'ID', 'TYPE', 'HREF', 'REL', 'DATA-SRC', 'SRC',
\ 'WIDTH', 'HEIGHT', 'XMLNS', 'MARGINHEIGHT', 'MARGINWIDTH', 'FRAMEBORDER',
\ 'SCROLLING', 'SPAN', 'CONTENT', 'NAME', 'VALUE' ]
function! WorkFixTags()
    " 1. replaces `<!DOCTYPE` with `<!doctype` on the first line in file
    " 2. replaces (eg) `<BODY>`/`<DIV`/`</DIV>` etc with lowercase equivalents
    " 3. replaces `ID="`/`CLASS="` etc with lowercase equivalents
    normal! mz
    1s/^<!\zs\(DOCTYPE\)\>/\L\1/Ie
    silent execute '%s/<\/\?\zs\('.join(g:workTagList, '\|').'\)\ze\>/\L\1/Ige'
    silent execute '%s/\<\('.join(g:workAttrList, '\|').'\)\>\ze="/\L\1/Ige'
    normal! `z
endfunction
" mnemonic: "work fix tags"
nnoremap <leader>wft :call WorkFixTags()<cr>

" nnoremap <leader>wT :%s/global\zs \ze= 1/Alpha / <bar> xa<cr>
" function! FixStupidness()
"   if search('^\s*_specCtx\.global = 1')
"       s/global\zs \ze= 1/Alpha /
"       write
"       qall
"   endif
" endfunction
" augroup TMP
"   autocmd FileType javascript call FixStupidness()
" augroup end

" WORK LOG
" ~~~~~~~~
" "<leader>wt" mnemonic: 'work time' (print hours worked and such)
nnoremap <silent> <leader>wt :pyfile $VIMDIR/python/workleft.py<cr>
" "<leader>wl" mnemonic: 'work log' (log in -or- log out)
nnoremap <silent> <leader>wl :pyfile $VIMDIR/python/worklog.py<cr>
" "<F21>"      convenience (Pause/Break key on razor black widow tournament ed)
nnoremap <silent> <F21> :pyfile $VIMDIR/python/worklog.py<cr>
" "<leader>wo" mnemonic: 'work open' (open current log)
nnoremap <silent> <leader>wo :pyfile $VIMDIR/python/workopen.py<cr>

command! -nargs=0 -bar Term execute '<mods> terminal sh -c ''bash -i''' <bar> call term_sendkeys(bufnr(''), 'cd '.expand('%:p:h:s?^\(\u\):?/mnt/\l\1?')."\<cr>\<c-l>")

command! -nargs=? -bar TT tab Term

" command! -complete=dir -nargs=? TCD lcd <args> | call term_sendkeys(bufnr(''), "\<c-e>\<c-u>cd ".<q-args>."\<cr>")
" function! TCD()
"   return join(map(term_scrape(bufnr(''), '.'), { _,v -> v.chars }), '') =~# '^[~/][^$]*\s\$\sc' ? "d \<c-w>:TCD \<c-d>" : 'd'
" endfunction
" if exists(':tnoremap')
"   tnoremap <expr> d TCD()
" endif

" command! -complete=file -nargs=? TVIM drop <args>
" function! TVIM()
"   return join(map(term_scrape(bufnr(''), '.'), { _,v -> v.chars }), '') =~# '^[~/][^$]*\s\$\svi' ? "\<c-e>\<c-u>\<c-w>:TVIM \<c-d>" : 'm'
" endfunction
" if exists(':tnoremap')
"   tnoremap <expr> m TVIM()
" endif

" nnoremap <silent> <leader>P :edit ~/Desktop/ProjectIndex.txt<cr>:setlocal nobuflisted<bar>if search('^Current') <bar> exe 'norm! +' <bar> else <bar> exe 'norm! 3G' <bar> endif<cr>
nnoremap <silent> <leader>P :edit ~/Desktop/ProjectIndex.txt<cr>

function! WorkReskinMicro2Olympus()
    " check if in right place
    first
    if expand('%:p') !~# '^X:\/Sites\/Java\/html5\/Olympus\/source\/'
        echohl ErrorMsg
        echon 'Not in Olympus - can''t reskin'
        echohl NONE
        return
    endif

    " load everything
    call WorkAddFiles(0)

    " html
    call WorkFixTags()
    silent normal! gg=G
    silent! RmTrailWS
    if getline(3) =~ '<head>' && getline(4) =~ '^\s*$'
        4delete
    endif
    if getline(7) =~ '<\/head>' && getline(6) =~ '^\s*$'
        6delete
    endif
    if search('\C\<controlBackContainer\>', 'n')
        silent %s/\C\<controlBack\>/controlPanelBack/ge
        silent %s/\C\<controlBackContainer\>/controlBack/ge
        silent execute 'buffer styles/'.expand('%:r').'.css'
        silent %s/\C\<controlBack\>/controlPanelBack/ge
        silent %s/\C\<controlBackContainer\>/controlBack/ge
        silent buffer#
    endif
    silent %s/<div\s\+class=\("tutorialTitle"\)>\(.\{-}\)<\/div>/<canvas id=\1>\2<\/canvas>/e
    silent %s/\<\%(blackText\|labelText\)\>/diagramTextBlack/ge
    silent 4change
    <link rel="stylesheet" type="text/css" href="/html5/Olympus/includes/styles/OlyUtil.css" />
    .
    silent %s/\/html5\/Micro\/includes/\/html5\/Olympus\/includes/ge
    silent %s/MicUtil/OlyUtil/ge
    if search('class="mainscene"') && ! search('splashContainer', 'n')
        normal ^l%
        let l:back = @"
        let @" = "\t\t<div id=\"splashContainer\"></div>"
        put!"
        let @" = l:back
    endif

    " css
    silent execute 'buffer styles/'.expand('%:r').'.css'
    silent! RmTrailWS
    silent normal! gg=G
    silent %s/\<\%(blackText\|labelText\)\>/diagramTextBlack/ge
    silent %s/^\zs\.mainscene\ze\s*{\s*$/body/e
    1
    if search('^body\s*{\n\s*\zswidth:')
        execute "normal f;ls\<cr>\<esc>kgccjotext-align: center;"
    endif
    if search('^\.controlPanelBack\s*\zs{')
        silent normal! di{k
        silent call append('.', map(['position: relative;', 'display: inline-block;', 'margin: 4px;', 'padding: 4px;', 'vertical-align: middle;'], { _,s -> "\t".s }))
        silent normal! 6jo
        silent call append('.', ['.controlText {', '    margin: 4px auto;', '}'])
    endif

    " js
    silent first
    silent execute 'buffer js/'.expand('%:r').'.js'
    silent normal! gg=G
    silent %s/\C\<Mic/Oly/ge
    silent %s/\<\%(blackText\|labelText\)\>/diagramTextBlack/ge

    " index.html
    silent last
    "" call WorkFixTags()
    "" retab!
    if search('micTutorial', 'n')
        let l:iframeLine = getline(search('^\s*<iframe'))
        let l:iframeAtts = [
        \   substitute(l:iframeLine, '.*\<src="\([^"]\{-}\)".*', '\1', ''),
        \   substitute(l:iframeLine, '.*\<width="\([^"]\{-}\)".*', '\1', ''),
        \   substitute(l:iframeLine, '.*\<height="\([^"]\{-}\)".*', '\1', '')
        \]
        silent %delete
        silent silent read X:/Sites/Java/html5/Olympus/OlyTemplate/The Template/index.html
        silent 1delete
        silent call search('^\s*<iframe')
        silent execute 's/src="\zs[^"]\{-}\ze"/'.escape(l:iframeAtts[0], '\/').'/e'
        silent execute 's/width="\zs[^"]\{-}\ze"/'.escape(l:iframeAtts[1], '\/').'/e'
        silent execute 's/height="\zs[^"]\{-}\ze"/'.escape(l:iframeAtts[2], '\/').'/e'
    endif

    silent wall
    silent first
endfunction
nnoremap <silent> <leader>wrs :call WorkReskinMicro2Olympus()<cr>


" ****************************************************************************

augroup Projects
    autocmd BufEnter ~/Desktop/ProjectIndex.txt setlocal nobuflisted filetype=lsbuffer concealcursor=nvic conceallevel=3
    autocmd BufEnter ~/Desktop/ProjectIndex.txt call matchadd('Conceal', '\%x00', -1)
    autocmd BufEnter ~/Desktop/ProjectIndex.txt call matchadd('Conceal', '^\%x00vim:.*:$', -1)
    autocmd BufEnter ~/Desktop/ProjectIndex.txt nnoremap <silent> <buffer> <expr>    l getline('.') =~# '^\%x00' ? 'l' : ':call lsbuffer#ls('''.getline('.').''')<cr>'
    autocmd BufEnter ~/Desktop/ProjectIndex.txt nnoremap <silent> <buffer> <expr> <cr> getline('.') =~# '^\%x00' ? 'l' : ':call lsbuffer#ls('''.getline('.').''')<cr>'
augroup end


function! WorkChangeAnonymous()
    if &ft ==# 'html'
        %s/^\s*<img.\{-}\zs[A-z\-.\/]\+\.\%(png\|jpg\|gif\)".*\%(\scrossorigin="Anonymous"\)\@<!\ze>\s*<\/img>\s*$/& crossorigin="Anonymous"/gce
        %s/^\s*<img.\{-}\zs[A-z\-.\/]\+\.\%(png\|jpg\)"\ze\%(\scrossorigin="Anonymous"\)\@!.\{-}\/>\s*$/& crossorigin="Anonymous"/gce
    elseif &ft ==# 'javascript'
        set cul
        g/\.src\s*=/redraw! | if getline(line('.') - 1) !~# 'crossorigin' | echo 'Prepend crosssorigin? ' | if nr2char(getchar()) !=? 'n' | execute "normal! _yt.O\<c-r>\".setAttribute(\"crossorigin\", \"Anonymous\");" | endif | endif
        set nocul
        let @/ = '/img\|src\|jpg\|png\|gif/'
        call feedkeys('n', 'mt')
    endif
endfunction
nnoremap <leader>wca :call WorkChangeAnonymous()<cr>


function! WorkAddTemplate()
    let l:dir = expand('%:p')
    if l:dir !~# '\/html5\/'
        echohl ErrorMsg
        echon 'not in tutorial directory (pass 1 to force)'
        echohl NONE
        return
    endif
    let l:d = split(l:dir, '\%(\\\)\@<!\%(\\\{2}\)*\zs/\+')
    let l:dir = escape(join(extend(l:d[:index(l:d, 'html5')+1], ['OlyTemplate', 'The Template']), '/'), '''')
    call system('cp -r '''.l:dir.'''/* .')
    call system('rm -f Thumbs.db **/Thumbs.db')
    let l:default = substitute(expand('%:p:h:t'), '-\w\+$', '', '')
    let l:name = input('name? ', l:default)
    call rename('mobile/tutName.html', 'mobile/'.l:name.'.html')
    call rename('mobile/js/tutName.js', 'mobile/js/'.l:name.'.js')
    call rename('mobile/styles/tutName.css', 'mobile/styles/'.l:name.'.css')
    lcd mobile
    if &filetype ==# 'lsbuffer'
        call lsbuffer#ls()
    endif
    argdel *
    argadd *.html
    first
    silent call WorkAddFiles(0)
endfunction
nnoremap <leader>wat :call WorkAddTemplate()<cr>


augroup StupidWindows
    autocmd!
    autocmd BufRead *.html if &readonly && input('reload buffer? ') =~? '^y\%[es]$' | call RefreshBuffer() | endif
augroup end
nnoremap <silent> <leader>R :call RefreshBuffer()<cr>
command! RefreshBuffer call RefreshBuffer()
function! RefreshBuffer()
    let l:tmp = tempname()
    silent! execute 'write '.l:tmp
    silent! execute '!mv '.l:tmp.' '.expand('%')
    silent! redraw!
    silent! edit!
    unsilent echon 'Refreshed'
endfunction


command! IRC if bufnr('irc') !=# -1 | if empty(win_findbuf(bufnr('irc'))) | botright sbuffer irc | exec '6wincmd _' | endif | else | botright Term | call term_sendkeys(bufnr('%'),"tmux attach -t irc\<cr>") | exec 'file irc' | endif | setl nobuflisted nonu nornu nolist winfixheight | 6wincmd _ | wincmd p

