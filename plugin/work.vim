
if !AtWork()
	finish
endif

" F11 fullscreen on windows
nnoremap <f11> :call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)<cr>
nnoremap <Del> :call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0) <bar> exec 'simalt ~n'<cr>:call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0) <bar> exec 'simalt ~n'<cr>
augroup GvimWin7
	autocmd!

	" fullscreen as soon as any file is opened
	" autocmd VimEnter * call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)
	autocmd VimEnter * simalt ~x
augroup end

function! WorkAddFiles()
	argadd styles/*.css js/*.js data/* ../index.html
	" args
endfunction

nnoremap <silent> <leader>aa :call WorkAddFiles()

" automagically open all the files I want for files that look like the projects
" I work on a lot
augroup Work
	autocmd!
	autocmd VimEnter *.html call <sid>OpenWork()
augroup end

function! <sid>OpenWork()
	if (argc() !=# 1)
		return
	endif
	let l:fmatch = '^'.expand('%:t:r')
	if (expand('%:p:h:h:t') =~ l:fmatch ||
	  \ expand('%:p:h:h:h:t') =~ l:fmatch ||
	  \ expand('%:p:h:h:h:h:t') =~ l:fmatch)
		call WorkAddFiles()
	endif
	silent call feedkeys(':buffer js/'.expand('%:t:r').".js\<cr>", 'n')
endfunction

nnoremap <leader>B :execute 'terminal bash' <bar> wincmd T<cr>

" ctags
nnoremap <leader>c :AsyncRun C:/Users/dmcclure/Downloads/ctags.exe -R .

