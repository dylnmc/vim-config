
" if italic causes reverse instead, command next line
let s:italic = 1

" nord
function! lyne#colors#nord()
	hi LyneModeInsert  guibg=#4c5668 ctermbg=8   guifg=#e5e9f0 ctermfg=7   gui=bold cterm=bold
	hi LyneModeNormal  guibg=#4c5668 ctermbg=8   guifg=#81a1c1 ctermfg=4   gui=bold cterm=bold
	hi LyneModeVisual  guibg=#4c5668 ctermbg=8   guifg=#b48ead ctermfg=5   gui=bold cterm=bold
	hi LyneModeReplace guibg=#4c5668 ctermbg=8   guifg=#bf616a ctermfg=1   gui=bold cterm=bold
	hi LyneModeCommand guibg=#4c5668 ctermbg=8   guifg=#a3be8c ctermfg=2   gui=bold cterm=bold
	hi LyneModePrompt  guibg=#4c5668 ctermbg=8   guifg=#ebcb8b ctermfg=3   gui=bold cterm=bold
	hi LyneModeTerm    guibg=#4c5668 ctermbg=8   guifg=#e5e9f0 ctermfg=7   gui=bold cterm=bold

	hi LyneBufnameUnmodified guibg=#4c5668 ctermbg=8 guifg=#e5e9f0 ctermfg=7
	hi LyneBufnameModified   guibg=#4c5668 ctermbg=8 guifg=#88c0d0 ctermfg=6

	hi StatusLine   guifg=#e5e9f0 ctermfg=7
	hi StatusLineNc guibg=#3b4252 ctermbg=0 guifg=#e5e9f0 ctermfg=7
	if get(s:, 'italic', 0)
		 hi StatusLineNC gui=italic cterm=italic
	 endif
	hi! link StatusLineTerm StatusLine
	hi! link StatusLineTermNC StatusLineNC
endfunction

" paramount
function! lyne#colors#paramount()
	hi LyneModeInsert  guibg=#303030 ctermbg=236 guifg=#f1f1f1 ctermfg=15  gui=bold cterm=bold
	hi LyneModeNormal  guibg=#303030 ctermbg=236 guifg=#20BBFC ctermfg=12  gui=bold cterm=bold
	hi LyneModeVisual  guibg=#303030 ctermbg=236 guifg=#a790d5 ctermfg=140 gui=bold cterm=bold
	hi LyneModeReplace guibg=#303030 ctermbg=236 guifg=#e32791 ctermfg=1   gui=bold cterm=bold
	hi LyneModeCommand guibg=#303030 ctermbg=236 guifg=#5fd7a7 ctermfg=10  gui=bold cterm=bold
	hi LyneModePrompt  guibg=#303030 ctermbg=236 guifg=#ffff87 ctermfg=228 gui=bold cterm=bold
	hi LyneModeTerm    guibg=#303030 ctermbg=236 guifg=#f1f1f1 ctermfg=15  gui=bold cterm=bold

	hi LyneBufnameUnmodified guibg=#303030 ctermbg=236 guifg=#E5E9F0 ctermfg=7
	hi LyneBufnameModified   guibg=#303030 ctermbg=232 guifg=#5fd7a7 ctermfg=235 gui=bold cterm=bold

	hi StatusLine guibg=#303030 ctermbg=236 guifg=#E5E9F0 ctermfg=7
	if get(s:, 'italic', 0)
		hi StatusLineNC gui=italic cterm=italic
	endif
	hi! link StatusLineTerm StatusLine
	hi! link StatusLineTermNC StatusLineNC
endfunction

" gruvbox
function! lyne#colors#gruvbox()
	hi LyneModeInsert  guibg=#3c3836 ctermbg=237 guifg=#fabd2f ctermfg=214 gui=bold cterm=bold
	hi LyneModeNormal  guibg=#3c3836 ctermbg=237 guifg=#83a598 ctermfg=109 gui=bold cterm=bold
	hi LyneModeVisual  guibg=#3c3836 ctermbg=237 guifg=#d3869b ctermfg=175 gui=bold cterm=bold
	hi LyneModeReplace guibg=#3c3836 ctermbg=237 guifg=#fb4934 ctermfg=167 gui=bold cterm=bold
	hi LyneModeCommand guibg=#3c3836 ctermbg=237 guifg=#8ec07c ctermfg=109 gui=bold cterm=bold
	hi LyneModePrompt  guibg=#3c3836 ctermbg=237 guifg=#ffff87 ctermfg=228 gui=bold cterm=bold
	hi LyneModeTerm    guibg=#3c3836 ctermbg=237 guifg=#fabd2f ctermfg=214 gui=bold cterm=bold

	hi LyneBufnameUnmodified guibg=#3c3836 ctermbg=237  guifg=#f2e5bc ctermfg=228
	hi LyneBufnameModified   guibg=#3c3836 ctermbg=237 guifg=#83a598 ctermfg=109

	hi StatusLine guibg=#3c3836 ctermbg=237 guifg=#bdae93 ctermfg=248
	hi StatusLineNC guibg=#504945 ctermbg=239 guifg=#bdae93 ctermfg=248
	if get(s:, 'italic', 0)
		hi StatusLineNC gui=italic cterm=italic
	endif
	hi! link StatusLineTerm StatusLine
	hi! link StatusLineTermNC StatusLineNC
endfunction

function! lyne#colors#gruvbox8()
	call lyne#colors#gruvbox()
endfunction

function! lyne#colors#gruvbox8_hard()
	call lyne#colors#gruvbox()
endfunction

function! lyne#colors#gruvbox8_soft()
	call lyne#colors#gruvbox()
endfunction

" badwolf
function! lyne#colors#badwolf()
	hi LyneModeInsert  guibg=#f8f6f2 ctermbg=15  guifg=#242321 ctermfg=235 gui=bold cterm=bold
	hi LyneModeNormal  guibg=#0a9dff ctermbg=39  guifg=#242321 ctermfg=235 gui=bold cterm=bold
	hi LyneModeVisual  guibg=#ff9eb8 ctermbg=211 guifg=#242321 ctermfg=235 gui=bold cterm=bold
	hi LyneModeReplace guibg=#ff2c4b ctermbg=196 guifg=#242321 ctermfg=235 gui=bold cterm=bold
	hi LyneModeCommand guibg=#aeee00 ctermbg=154 guifg=#242321 ctermfg=235 gui=bold cterm=bold
	hi LyneModePrompt  guibg=#fade3e ctermbg=221 guifg=#242321 ctermfg=235 gui=bold cterm=bold
	hi LyneModeTerm    guibg=#f8f6f2 ctermbg=15  guifg=#242321 ctermfg=235 gui=bold cterm=bold

	hi LyneBufnameUnmodified guibg=#45413b ctermbg=238 guifg=#d9cec3 ctermfg=252
	hi LyneBufnameModified   guibg=#45413b ctermbg=238 guifg=#aeee00 ctermfg=154

	hi StatusLine   guibg=#35322d ctermbg=236 guifg=#d9cec3 ctermfg=252 gui=NONE cterm=NONE
	hi StatusLineNC guibg=#35322d ctermbg=236 guifg=#d9cec3 ctermfg=252
	if get(s:, 'italic', 0)
		hi StatusLineNC gui=italic cterm=italic
	endif
	hi! link StatusLineTerm StatusLine
	hi! link StatusLineTermNC StatusLineNC
endfunction

" jellybeans
function! lyne#colors#jellybeans()
	hi LyneModeInsert  guibg=#dad085 ctermbg=159 guifg=#1c1c1c ctermfg=0   gui=bold cterm=bold
	hi LyneModeNormal  guibg=#7697d6 ctermbg=5   guifg=#1c1c1c ctermfg=0   gui=bold cterm=bold
	hi LyneModeVisual  guibg=#f0a0c0 ctermbg=0   guifg=#302028 ctermfg=11  gui=bold cterm=bold
	" hi LyneModeReplace guibg=#902020 ctermbg=9   guifg=#1c1c1c ctermfg=0   gui=bold cterm=bold
	hi LyneModeCommand guibg=#70b950 ctermbg=2   guifg=#242321 ctermfg=235 gui=bold cterm=bold
	" hi LyneModePrompt  guibg=#fade3e ctermbg=221 guifg=#242321 ctermfg=235 gui=bold cterm=bold
	hi LyneModeTerm    guibg=#dad085 ctermbg=159 guifg=#1c1c1c ctermfg=0   gui=bold cterm=bold

	hi LyneBufnameUnmodified guibg=#32302f ctermbg=236  guifg=#f2e5bc ctermfg=228
	hi LyneBufnameModified   guibg=#32302f ctermbg=236 guifg=#83a598 ctermfg=109

	hi StatusLine guifg=#ffffff guibg=#403c41 ctermbg=235
	if get(s:, 'italic', 0)
		hi StatusLineNC gui=italic cterm=italic
	endif
	hi! link StatusLineTerm StatusLine
	hi! link StatusLineTermNC StatusLineNC
endfunction

" onedark
function! lyne#colors#onedark()
	hi LyneModeInsert  guibg=#2C323C ctermbg=236 guifg=#abb2bf ctermfg=145 gui=bold cterm=bold
	hi LyneModeNormal  guibg=#2C323C ctermbg=236 guifg=#61afef ctermfg=39  gui=bold cterm=bold
	hi LyneModeVisual  guibg=#2C323C ctermbg=236 guifg=#c678dd ctermfg=170 gui=bold cterm=bold
	hi LyneModeReplace guibg=#2C323C ctermbg=236 guifg=#e06c75 ctermfg=196 gui=bold cterm=bold
	hi LyneModeCommand guibg=#2C323C ctermbg=236 guifg=#56b6c2 ctermfg=38  gui=bold cterm=bold
	hi LyneModePrompt  guibg=#2C323C ctermbg=236 guifg=#e5c07b ctermfg=180 gui=bold cterm=bold
	hi LyneModeTerm    guibg=#2C323C ctermbg=236 guifg=#dad085 ctermfg=159 gui=bold cterm=bold

	hi LyneBufnameUnmodified guibg=#2C323C ctermbg=236 guifg=#abb2bf ctermfg=145
	hi LyneBufnameModified   guibg=#2C323C ctermbg=236 guifg=#61afef ctermfg=39  gui=bold cterm=bold

	" hi StatusLine guibg=#2c323c ctermbg=236 guifg=#abb2bf ctermfg=145
	hi StatusLineNc guifg=#6c7380
	" if get(s:, 'italic', 0)
	" 	hi StatusLineNC gui=italic cterm=italic
	" endif
	hi! link StatusLineTerm StatusLine
	hi! link StatusLineTermNC StatusLineNC
endfunction

" igemnace
function! lyne#colors#igemnace()
	hi LyneModeInsert  guibg=#515151 ctermbg=8 guifg=#d3d0c8 ctermfg=7 gui=bold cterm=bold
	hi LyneModeNormal  guibg=#515151 ctermbg=8 guifg=#6699cc ctermfg=4 gui=bold cterm=bold
	hi LyneModeVisual  guibg=#515151 ctermbg=8 guifg=#cc99cc ctermfg=5 gui=bold cterm=bold
	hi LyneModeReplace guibg=#515151 ctermbg=8 guifg=#f2777a ctermfg=1 gui=bold cterm=bold
	hi LyneModeCommand guibg=#515151 ctermbg=8 guifg=#99cc99 ctermfg=2 gui=bold cterm=bold
	hi LyneModePrompt  guibg=#515151 ctermbg=8 guifg=#cc99cc ctermfg=5 gui=bold cterm=bold
	hi LyneModeTerm    guibg=#515151 ctermbg=8 guifg=#d3d0c8 ctermfg=7 gui=bold cterm=bold

	hi LyneBufnameUnmodified guibg=#515151 ctermbg=8 guifg=#99cc99 ctermfg=2
	hi LyneBufnameModified   guibg=#515151 ctermbg=8 guifg=#99cc99 ctermfg=2 gui=bold cterm=bold

	" hi LyneSeparator guibg=#515151 ctermbg=8 guifg=#6699cc ctermfg=4

	" hi StatusLine guifg=#d3d0c8 guibg=#515151
	hi StatusLine ctermbg=8 ctermfg=15
	if get(s:, 'italic', 0)
		hi StatusLineNC gui=italic cterm=italic
	endif
	hi! link StatusLineTerm StatusLine
	hi! link StatusLineTermNC StatusLineNC
endfunction

" novum
function! lyne#colors#novum()
	exec printf('hi LyneModeNormal  guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:novum#xterm_colors[g:novum#colors.mutedgreen],
	\ g:novum#colors.mutedgreen,
	\ g:novum#xterm_colors[g:novum#colors.bg],
	\ g:novum#colors.bg,
	\)
	exec printf('hi LyneModeInsert  guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:novum#xterm_colors[g:novum#colors.mutedyellow],
	\ g:novum#colors.mutedyellow,
	\ g:novum#xterm_colors[g:novum#colors.bg],
	\ g:novum#colors.bg,
	\)
	exec printf('hi LyneModeVisual  guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:novum#xterm_colors[g:novum#colors.purple],
	\ g:novum#colors.purple,
	\ g:novum#xterm_colors[g:novum#colors.bg],
	\ g:novum#colors.bg,
	\)
	exec printf('hi LyneModeReplace guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:novum#xterm_colors[g:novum#colors.red],
	\ g:novum#colors.red,
	\ g:novum#xterm_colors[g:novum#colors.bg],
	\ g:novum#colors.bg,
	\)
	exec printf('hi LyneModeCommand guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:novum#xterm_colors[g:novum#colors.green],
	\ g:novum#colors.green,
	\ g:novum#xterm_colors[g:novum#colors.bg],
	\ g:novum#colors.bg,
	\)
	exec printf('hi LyneModePrompt  guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:novum#xterm_colors[g:novum#colors.orange],
	\ g:novum#colors.orange,
	\ g:novum#xterm_colors[g:novum#colors.bg],
	\ g:novum#colors.bg,
	\)
	exec printf('hi LyneModeTerm    guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:novum#xterm_colors[g:novum#colors.mutedyellow],
	\ g:novum#colors.mutedyellow,
	\ g:novum#xterm_colors[g:novum#colors.bg],
	\ g:novum#colors.bg,
	\)

	exec printf('hi LyneBufnameUnmodified guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:novum#xterm_colors[g:novum#colors.darkfg],
	\ g:novum#colors.darkfg,
	\ g:novum#xterm_colors[g:novum#colors.bg],
	\ g:novum#colors.bg,
	\)
	exec printf('hi LyneBufnameModified   guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:novum#xterm_colors[g:novum#colors.mutedblue],
	\ g:novum#colors.mutedblue,
	\ g:novum#xterm_colors[g:novum#colors.bg],
	\ g:novum#colors.bg,
	\)
    exec printf('hi StatusLine guibg=%s ctermbg=%d guifg=%s ctermfg=%d',
    \ g:novum#xterm_colors[g:novum#colors.uibg],
    \ g:novum#colors.uibg,
    \ g:novum#xterm_colors[g:novum#colors.uifg],
    \ g:novum#colors.uifg
    \)
    hi StatusLineNC gui=bold cterm=bold
endfunction

" silentium
function! lyne#colors#silentium()
	exec printf('hi LyneModeNormal  guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:silentium#xterm_colors[g:silentium#colors.uibg],
	\ g:silentium#colors.uibg,
	\ g:silentium#xterm_colors[g:silentium#colors.blue],
	\ g:silentium#colors.blue
	\)
	exec printf('hi LyneModeInsert  guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:silentium#xterm_colors[g:silentium#colors.uibg],
	\ g:silentium#colors.uibg,
	\ g:silentium#xterm_colors[g:silentium#colors.yellow],
	\ g:silentium#colors.yellow
	\)
	exec printf('hi LyneModeVisual  guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:silentium#xterm_colors[g:silentium#colors.uibg],
	\ g:silentium#colors.uibg,
	\ g:silentium#xterm_colors[g:silentium#colors.purple],
	\ g:silentium#colors.purple
	\)
	exec printf('hi LyneModeReplace guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:silentium#xterm_colors[g:silentium#colors.uibg],
	\ g:silentium#colors.uibg,
	\ g:silentium#xterm_colors[g:silentium#colors.red],
	\ g:silentium#colors.red
	\)
	exec printf('hi LyneModeCommand guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:silentium#xterm_colors[g:silentium#colors.uibg],
	\ g:silentium#colors.uibg,
	\ g:silentium#xterm_colors[g:silentium#colors.green],
	\ g:silentium#colors.green
	\)
	exec printf('hi LyneModePrompt  guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:silentium#xterm_colors[g:silentium#colors.uibg],
	\ g:silentium#colors.uibg,
	\ g:silentium#xterm_colors[g:silentium#colors.orange],
	\ g:silentium#colors.orange
	\)
	exec printf('hi LyneModeTerm    guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:silentium#xterm_colors[g:silentium#colors.uibg],
	\ g:silentium#colors.uibg,
	\ g:silentium#xterm_colors[g:silentium#colors.fg],
	\ g:silentium#colors.fg
	\)

	exec printf('hi LyneBufnameUnmodified guibg=%s ctermbg=%d guifg=%s ctermfg=%d',
	\ g:silentium#xterm_colors[g:silentium#colors.uibg],
	\ g:silentium#colors.uibg,
	\ g:silentium#xterm_colors[g:silentium#colors.lightgreen],
	\ g:silentium#colors.lightgreen
	\)
	exec printf('hi LyneBufnameModified   guibg=%s ctermbg=%d guifg=%s ctermfg=%d gui=bold cterm=bold',
	\ g:silentium#xterm_colors[g:silentium#colors.uibg],
	\ g:silentium#colors.uibg,
	\ g:silentium#xterm_colors[g:silentium#colors.lightgreen],
	\ g:silentium#colors.lightgreen
	\)
endfunction
