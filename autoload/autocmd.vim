
function! autocmd#nomkview()
  if !exists('b:nomkview')
    let b:nomkview = index(get(g:, 'nomkview_fts', []), &ft) isnot -1
  endif
  return get(g:, 'nomkview') || get(w:, 'nomkview') || get(b:, 'nomkview')
endfunction

function! autocmd#HandleSwap(filename)
    let pid = string(swapinfo(v:swapname).pid)
    let ppid = '(pid: '.pid.')'
    let fname = '"'.fnameescape(expand('%')).'"'
    echohl WarningMsg
    redraw
    if getftime(v:swapname) < getftime(a:filename)
        " delete swap file if older than file itself; then edit
        let v:swapchoice = 'e'
        unsil echom 'Old swap deleted:' fname ppid
        call delete(v:swapname)
    else
        let haspgrep = executable('pgrep')
        if haspgrep && index(systemlist('pgrep -x vim'), pid) >= 0
            " file open in another vim; edit
            let v:swapchoice = 'e'
            unsil echom 'Already editing' fname ppid
        else
            let v:swapchoice = 'o'
            " open read-only:
            "   if haspgrep, almost definitely a crash
            "   else, either crash or file open but can't discover (with pgrep)
            let out = haspgrep ? 'Crash' : 'Swapfile'
            let note = '-> use :DiffOrig and :recover if needed'
            unsil echom  out 'detected' ppid 'for' fname note
        endif
    endif
    echohl NONE
endfunction

