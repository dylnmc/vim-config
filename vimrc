"+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+"
"|                  https://bitbucket.org/dylnmc/vim-config                   |"
"+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+"
"|    "gf" to jump to file in vim     |             "description"             |"
"+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+"
"| $VIMDIR/plugin/color.vim           | "color settings"                      |"
"| $VIMDIR/plugin/maps.vim            | "everyday mappings"                   |"
"| $VIMDIR/plugin/plugin-settings.vim | "settings for all my plugins"         |"
"| $VIMDIR/plugin/save.vim            | "how I save files"                    |"
"| $VIMDIR/plugin/search.vim          | "* and # searching FIXED!"            |"
"| $VIMDIR/plugin/synstack.vim        | "simple way to view highlight groups" |"
"| $VIMDIR/plugin/work.vim            | "work stuff"                          |"
"| $VIMDIR/after/plugin/fold.vim      | "folding"                             |"
"+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+"
"|        git clone https://git::@bitbucket.org:dylnmc/vim-config.git         |"
"+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+"

set shellslash " need this here for windows (so that expand($HOME) has / not \)
let $VIMDIR = expand($HOME) . (has('win32') ? '/vimfiles' : '/.vim')

if (has('win32') || has('multibyte'))
	set encoding=utf-8
	scriptencoding utf-8
endif

function! AtWork()
	return hostname() ==# 'GRAPHICS32'
endfunction

filetype plugin indent on

set autoread autowrite
set backspace=indent,eol,start
set cmdheight=1 " set this to 2 if getting a lot of "Press Enter" prompts
set colorcolumn=80,140,200
set complete+=i " completion from included files
set formatoptions+=j " delete comment when joining
set hidden
set ignorecase " use /\C for case-sensitive (also plugin/search.vim)
set laststatus=2
set lazyredraw
set list
set listchars=trail:·,tab:\›\ ,nbsp:~ " tab alternatives: ┃│┊›▸
set mouse= " f the mouse
set noerrorbells novisualbell t_vb= belloff=all
set noexpandtab
set nojoinspaces
set noshowmode
set nostartofline " keep cursor in column when jumping
set number relativenumber
set path+=**
set scrolloff=1
set shiftround
set shortmess=aAIqFcTWsqt
set splitbelow splitright
set tabstop=4 shiftwidth=4 softtabstop=4
set textwidth=80
set tildeop
set ttimeoutlen=20 " stop stl lag
set wildcharm=<c-z>
if has('cmdline_hist')
	set history=10000 " max
endif
if has('cmdline_info')
	set showcmd
endif
if has('diff')
	set diffopt+=vertical " vertical split diffs
	if has('win32')
		set showbreak=+
	else
		set showbreak=↪
	endif
endif
if has('eval') && has('insert_expand')
	set omnifunc=syntaxcomplete#Complete
endif
if has('extra_search')
	set hlsearch incsearch
endif
if has('folding')
	set foldlevel=1
	set foldmethod=syntax
endif
if has('linebreak')
	set breakindent
endif
if has('syntax')
	set synmaxcol=200
	set nocursorline
	syntax enable
endif
if has('virtualedit')
	set virtualedit=block,insert
endif
if has('wildmenu')
	set wildmenu
endif

" create undo, backup, tmp dirs for vim if they don't exist
if !isdirectory($VIMDIR.'/undo')   | call mkdir($VIMDIR.'/undo', 'p')   | endif
if !isdirectory($VIMDIR.'/backup') | call mkdir($VIMDIR.'/backup', 'p') | endif
if !isdirectory($VIMDIR.'/swap')   | call mkdir($VIMDIR.'/swap', 'p')   | endif

" don't clutter current dir but use awesome functionality that vim provides
" undo
if has('persistent_undo')
	set undodir=$VIMDIR/undo//
	set undofile
endif
" backup
set backupdir=$VIMDIR/backup//
set backup
" swap
set directory=$VIMDIR/swap//
set swapfile
" set viminfo file to be inside $VIMDIR
execute 'set viminfo+=n'.$VIMDIR.'/viminfo'

if !isdirectory($VIMDIR.'/autoload')
	call mkdir($VIMDIR.'/autoload', 'p')
endif
if empty(glob($VIMDIR.'/autoload/plug.vim'))
	execute 'silent !curl -fLo '.$VIMDIR.'/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
" plug
call plug#begin($VIMDIR.'/plugged')

" bitbucket
let g:plug_url_format = 'https://git::@bitbucket.org/%s.git'
Plug 'dylnmc/vim-logipair'
Plug 'dylnmc/vim-ultisnippets'
" Plug 'dylnmc/lyne-vim'

" github
unlet g:plug_url_format
if has('python')
	Plug 'SirVer/ultisnips'
endif

Plug 'arcticicestudio/nord-vim', {'branch': 'develop'}
Plug 'chrisbra/colorizer', {'on': ['ColorToggle', 'ColorHightlight']}
Plug 'godlygeek/tabular'
Plug 'heavenshell/vim-jsdoc', {'for': 'javascript'}
Plug 'jreybert/vimagit'
Plug 'lervag/vimtex', {'for': 'latex'}
Plug 'mbbill/undotree'
Plug 'morhetz/gruvbox' " backup colo
Plug 'neovimhaskell/haskell-vim', {'for': 'haskell'}
Plug 'pangloss/vim-javascript', {'for': 'javascript'}
Plug 'sickill/vim-pasta'
Plug 't9md/vim-quickhl' | Plug 'kana/vim-operator-user'
Plug 'tommcdo/vim-exchange'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-markdown'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'vim-airline/vim-airline',
Plug 'vim-scripts/ReplaceWithRegister'
Plug 'w0rp/ale'

if AtWork()
	Plug 'skywind3000/asyncrun.vim'
	Plug 'derekmcloughlin/gvimfullscreen_win32'
endif

call plug#end()

augroup Vimrc
	autocmd!

	" real tabs in make files
	autocmd FileType make,cmake setlocal noexpandtab

	" restore cursor position when reopening a file
	autocmd BufReadPost * call setpos(".", getpos("'\""))

	" *don't* insert comment when using o/O in normal mode
	" *do* insert comment when pressing <cr> in insert mode
	autocmd FileType * setlocal formatoptions+=c
	autocmd FileType * setlocal formatoptions-=o

	" close preview window on CompleteDone
	autocmd CompleteDone * pclose

	" open folds with incsearch
	try
		autocmd cmdlinechanged * if expand('<afile>') =~ '[/?]' |
				\ silent execute 'normal! zv' |
			\endif
	catch /^Vim\%((\a\+)\)\=:E216/
	endtry

	" always set textwidth to 80
	autocmd FileType * set textwidth=80

	autocmd FileType * try | set fileencoding=utf-8 |
		\ catch /^Vim\%((\a\+)\)\=:E21/ | endtry

	" prevent folding madness when typing parens or brackets in insert mode
	autocmd InsertEnter * let b:fdm = &foldmethod | set foldmethod=manual
	autocmd InsertLeave * execute 'set foldmethod='.get(b:, 'fdm', 'syntax')

augroup end

